$(document).ready(function () {
  $("#mdp").mdp();
});

(function ($) {
  /**
   * Создает экземпляр MDP.
   */
  $.fn.mdp = function (options) {
    var settings = $.extend(
      {
        buttonRun: "#BtnMdp",
        positionStart: "mdp-position-0",
      },
      options
    );

    var stackPosition = [];
    stackPosition.push(settings.positionStart);
    $("#" + settings.positionStart).addClass("active");

    var stackMsg = [];
    var stackDiscount = [];

    var buttonPrev = $(".mdp .btn-prev");
    var buttonNext = $(".mdp .btn-next");

    $(settings.buttonRun).click(function (e) {
      e.preventDefault();
      $("body").addClass("mdp-active");
    });

    $(".mdp .btn-close").click(function (e) {
      e.preventDefault();
      $("body").removeClass("mdp-active");
    });

    $(".mdp .mdp-content-item").click(function (e) {
      let tag = $(this).attr("mdp-data-tag");
      let prev = stackPosition.pop();
      let next = $(this).attr("mdp-data-position");
      stackPosition.push(prev);
      stackPosition.push(next);
      $("#" + prev).removeClass("active");
      $("#" + next).addClass("active");
      stackMsg.push(tag);
      discount($(this).attr("mdp-data-discord"));
      if (next == "mdp-position-end") {
        let msg = "";
        while (stackMsg.length > 0) {
          msg += stackMsg.shift() + " | ";
        }
        $("form.mdp-form .send").removeClass("hidden");
        $("#InputMsg").val(msg);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        alert(msg);
      }
    });

    $(".mdp-content-select").click(function (e) {
      $(this).toggleClass("selected");
    });

    /**
     * Клик по кнопке "Назад"
     */
    $(buttonPrev).click(function (e) {
      let prev = stackPosition.pop();
      if (prev == "mdp-position-0") {
        stackPosition.push(prev);
        return;
      }
      let next = stackPosition.pop();
      stackPosition.push(next);
      stackMsg.pop();
      discountMinus();
      $("#" + prev).removeClass("active");
      $("#" + next).addClass("active");
    });

    /**
     * Клик по кнопке "Далее"
     */
    $(buttonNext).click(function (e) {
      let inputs = $(".mdp-position.active input");
      let tag = "";
      if (inputs.length > 0) {
        for (let i = 0; i < inputs.length; i++) {
          let input = $(inputs[i]);
          if ($(input).val() == "") {
            $(input).addClass("error");
            return;
          }
          tag += $(input).attr("mdp-data-tag") + ": " + $(input).val() + " ";
        }
        stackMsg.push(tag);
        let prev = stackPosition.pop();
        let next = $(inputs[0]).attr("mdp-data-position");
        stackPosition.push(prev);
        stackPosition.push(next);
        $("#" + prev).removeClass("active");
        $("#" + next).addClass("active");
        discount($(this).attr("mdp-data-discord"));
      }
      let selects = $(".mdp-position.active .mdp-content-select.selected");
      if (selects.length > 0) {
        console.log(selects);
        let prev = stackPosition.pop();
        let next = $(selects[0]).attr("mdp-data-position");
        stackPosition.push(prev);
        stackPosition.push(next);
        let tag = "";
        for (let i = 0; i < selects.length; i++) {
          let select = $(selects[i]);
          tag += $(select).attr("mdp-data-tag") + ": " + $(select).val();
        }
        stackMsg.push(tag);
        $("#" + prev).removeClass("active");
        $("#" + next).addClass("active");
        discount($(this).attr("mdp-data-discord"));
      }
    });

    /**
     * Повысить скидку
     */
    function discount(discord) {
      let current = $("#InputDiscord").val();
      let next = parseInt(current) + parseInt(discord);
      $("#InputDiscord").val(next);
      $("#SpanDiscount").html(next);
      stackDiscount.push(discord);
    }
    /**
     * Снизить скидку
     */
    function discountMinus() {
      console.log(stackDiscount);
      let current = $("#InputDiscord").val();
      console.log("current: " + current);

      let pre = stackDiscount.pop();
      console.log("pre: " + pre);

      let next = parseInt(current) - parseInt(pre);
      if (stackDiscount.length == 0) {
        stackDiscount.push(0);
      }
      $("#InputDiscord").val(next);
      $("#SpanDiscount").html(next);
    }
  };
})(jQuery);
